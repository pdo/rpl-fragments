* -*- mode: sysrpl -*-
*
* ENGD -- Engineering display
*
* ( % -> % )
*
* Temporarily display a real number in ``engineering'' format.
*
* Allows adjustment of the exponent in multiples of three using the
* arrow keys or softmenu keys.
*
* Preserves the stack.
*

* Working definitions (setup by Makefile for appropriate calculator)
INCLUDE work/keyplanes.s
INCLUDE work/keycodes.s

* Local object (shortened) identifiers
DEFINE Expt          E
DEFINE Mant          M
DEFINE ShiftLimiter  SL
DEFINE IncrEng       I
DEFINE DecrEng       D
DEFINE ShouldQuit    SQ
DEFINE HandleDisplay HD
DEFINE HandleKey     HK
DEFINE SoftkeyDefs   SD
DEFINE ProgMain      PM

* Working header (setup by Makefile for appropriate calculator)
INCLUDE work/header.s
::
  ( Claim errors, check for one argument )

  0LASTOWDOB!
  CK1NOLASTWD

  ( Local objects )

  ( ShouldQuit ) ( -> T/F )
  FALSE

  ( Mant ) ( -> % )
  %0

  ( Expt ) ( -> % )
  %0

  ( ShiftLimiter ) ( -> # )
  BINT3
  
  ( IncrEng ) ( -> )
  ' ::
      LAM ShiftLimiter BINT5 #>?SKIP                  ( when shift not too big )
        ::
          LAM Expt %3 %+ ' LAM Expt STO               ( increase exponent by 3 )
          LAM Mant % 1000 %/ ' LAM Mant STO           ( divide mantissa by 1000 )
          LAM ShiftLimiter #1+ ' LAM ShiftLimiter STO ( increase shift value )
        ;
    ;

  ( DecrEng ) ( -> )
  ' ::
      BINT1 LAM ShiftLimiter #>?SKIP                  ( when shift not too small )
        ::
          LAM Expt %3 %- ' LAM Expt STO               ( decrease exponent by 3 )
          LAM Mant % 1000 %* ' LAM Mant STO           ( multiply mantissa by 1000 )
          LAM ShiftLimiter #1- ' LAM ShiftLimiter STO ( decrease shift value )
        ;
    ;

  ( HandleDisplay ) ( -> )
  ' ::
      DA3OK? ?SKIP :: DispMenu.1 SetDA3Valid ;
      savefmt1 %9 xFIX                            ( save current number format, use FIX 9 )
      NULL$ DISPROW3                              ( blank out row 3 )
      LAM Mant a%>$                               ( string representation of mantissa )
      DUPLEN$ BINT17 SWAP #- Blank$ SWAP &$       ( right justify by an appropriate amount )
      CHR_Space >T$                               ( a space to separate mantissa and exponent )
      LAM Expt %0< ITE CHR_- CHR_Space >T$        ( a minus sign if exponent is negative ) 
      LAM Expt %ABS COERCE #>$ &$                 ( absolute value of exponent )
      DISPROW4                                    ( show string on row 4 )
      NULL$ DISPROW5                              ( blank out row 5 )
      rstfmt1                                     ( restore number format )
    ;

  ( HandleKey ) ( #keycode #plane -> obj TRUE )
                ( #keycode #plane -> FALSE )
  ' ::
      kpNoShift #=casedrop                        ( unshifted keys )
        ::
          DUP#<7 casedrpfls                       ( enable primary softkeys )
          keyLeftShift #=casedrpfls               ( enable left-shift )
          keyRightShift #=casedrpfls              ( enable right-shift )
          keyLeftArrow ?CaseKeyDef                ( LeftArrow does IncrEng )
            :: TakeOver LAM IncrEng EVAL ;
          keyRightArrow ?CaseKeyDef               ( RightArrow does DecrEng )
            :: TakeOver LAM DecrEng EVAL ;
          keyCancel ?CaseKeyDef                   ( CANCEL exits program )
            :: TakeOver TRUE ' LAM ShouldQuit STO ;
          keyEnter ?CaseKeyDef                    ( ENTER exits program )
            :: TakeOver TRUE ' LAM ShouldQuit STO ;
          DROP 'DoBadKey TRUE                     ( reject other keys )
        ;
      kpLeftShift #=casedrop                      ( left-shifted keys )
        ::
          keyLeftShift #=casedrpfls               ( enable left-shift )
          keyRightShift #=casedrpfls              ( enable right-shift )
          DROP 'DoBadKey TRUE                     ( reject other keys )
        ;
      kpRightShift #=casedrop                     ( right-shifted keys )
        ::
          keyLeftShift #=casedrpfls               ( enable left-shift )
          keyRightShift #=casedrpfls              ( enable right-shift )
          keyCancel #=casedrpfls                  ( enable RS-[OFF] )
          DROP 'DoBadKey TRUE                     ( reject other keys )
        ;
      2DROP 'DoBadKey TRUE                        ( reject other plains )
    ;

  ( SoftkeyDefs ) ( -> list )
  { { "<" :: TakeOver LAM IncrEng EVAL ; }
    { ">" :: TakeOver LAM DecrEng EVAL ; }
    NullMenuKey
    NullMenuKey
    NullMenuKey
    { "Quit" :: TakeOver TRUE ' LAM ShouldQuit STO ; } }

  ( ProgMain ) ( % -> % )
  ' ::
      ( Initialize local variables )
      DUP %MANTISSA OVER %SGN %* ' LAM Mant STO   ( store signed mantissa )
      DUP %EXPONENT ' LAM Expt STO                ( store exponent )
      LAM Expt %3 %MOD                            ( correction factor for initial ENG display )
      DUP LAM Expt SWAP %- ' LAM Expt STO         ( initialize the exponent )
      %ALOG LAM Mant %* ' LAM Mant STO            ( initialize the mantissa )

      ( Setup POL parameters )
      LAM HandleDisplay                           ( the display object )
      LAM HandleKey                               ( the hardkey handler )
      TRUE                                        ( the nonappkey flag )
      TRUE                                        ( the dostdkeys flag )
      LAM SoftkeyDefs                             ( the softkey definitions )
      ONE                                         ( the initial menu page )
      FALSE                                       ( the suspend flag )
      ' LAM ShouldQuit                            ( the exit object )
      ' ERRJMP                                    ( the error object )

      ( Invoke the POL )
      ParOuterLoop

      ( Signal to redraw display )
      ClrDAsOK
    ;

  ( Name and bind the local objects )

  { LAM ShouldQuit LAM Mant LAM Expt LAM ShiftLimiter
    LAM IncrEng LAM DecrEng
    LAM HandleDisplay LAM HandleKey LAM SoftkeyDefs
    LAM ProgMain }

  BIND

  ( Dispatch to main routine according to stack object type )

  ::
    CK&DISPATCH1
    real :: LAM ProgMain EVAL ;
  ;

  ( Release local bindings and exit )

  ABND
;
