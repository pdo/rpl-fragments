*
* Key codes for the HP48G
*
DEFINE keyLeftArrow  BINT16
DEFINE keyRightArrow BINT18
DEFINE keyEnter      BINT25
DEFINE keyLeftShift  BINT35
DEFINE keyRightShift BINT40
DEFINE keyCancel     BINT45
