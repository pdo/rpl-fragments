*
* Key codes for the HP50G
*
DEFINE keyLeftArrow  BINT14
DEFINE keyRightArrow BINT16
DEFINE keyEnter      BINT51
DEFINE keyLeftShift  BINT37
DEFINE keyRightShift BINT42
DEFINE keyCancel     BINT47
