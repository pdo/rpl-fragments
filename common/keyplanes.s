*
* Key plane definitions
*

* Primary planes
DEFINE kpNoShift    BINT1
DEFINE kpLeftShift  BINT2
DEFINE kpRightShift BINT3

* Alpha planes
DEFINE kpANoShift    BINT4
DEFINE kpALeftShift  BINT5
DEFINE kpARightShift BINT6
